<?php
$title = 'Machines';
$longTitle = 'Gestion des machines';
include('../header.inc.php');
?>
<p>Voici le composant serveur de mon projet « Machines », qui me permet d'améliorer la sécurité des ordinateurs et serveurs sur lequel j'ai un accès en définissant des règles d'autorisation de connexion avec une clef SSH par machine, tout en facilitant la connexion aux machines par la génération automatique de fichiers de configuration SSH.</p>
<p>Bien que ce domaine ne soit pas d'une grande utilité pour vous, vous pouvez étudier le code si vous le désirez :
        <ul class="fa-ul" >
            <li><a href="https://git.frogeye.fr/geoffrey/dotfiles/src/master/scripts/machines.sh"><i class="fa fa-li fa-git"></i>&nbsp; Composante client</a></li>
            <li>
                <i class="fa fa-li fa-lock"></i>
                &nbsp; Composante serveur <br/>
                <em>(pas encore ouverte pour des raisons de sécurité)</em>
            </li>
        </ul>
</p>
<p>Malheureusement, comme la plupart des projets que je réalise pour moi-même, le code n'est pas très bien commenté, cela est notamment dû au manque d'objectif précis et de cahier des charges lors de la rédaction du code. Cependant, si vous êtes interessés par le système, <a href="mailto:geoffrey@frogeye.fr">dites-le moi</a> et je ferais un effort de clarification.</p>
<?php
include('../footer.inc.php');
?>
